<?php
	$monFichier = 'listeRessources.txt';
	$monTableau = array();

	// 1 : Lecture du fichier
	if (file_exists($monFichier)) {
		$pointeurFichier = fopen($monFichier, 'r')
			or die("Impossible d'ouvrir le fichier $monFichier !");
			while (!feof($pointeurFichier)) {
				$ligneFichier=trim(fgets($pointeurFichier));
				if (!empty($ligneFichier)) {
					$ligne = explode('|',$ligneFichier);
					$monTableau[$ligne[0]]=intval($ligne[1]);
			}
		}
	}
	fclose($pointeurFichier);
	arsort($monTableau,SORT_NUMERIC);

	// 2 : Lecture des paramètres
	if (!empty($_REQUEST['Voter']) && !empty($_REQUEST['bars'])) {
		foreach ($_REQUEST['bars'] as $numero => $bar) {
			$bardecode = htmlspecialchars_decode($bar);
			$monTableau[$bar]++;
		}
	}
	$nouveauBar = '';
	if (!empty($_REQUEST['Envoyer']) && !empty($_REQUEST['NouveauBar']) && !empty($_REQUEST['NouveauURL']) && !preg_match('/</',$_REQUEST['NouveauBar'])) {
		$nouveauBar = $_REQUEST['NouveauBar'];
		$nouveauBar = preg_replace('/</','',$nouveauBar);
		$nouveauBar = preg_replace('/"/','&quot;',$nouveauBar);
		$url = trim($_REQUEST['NouveauURL']);
		$nouveauBar = trim($nouveauBar);
		$nouveauBar = "<a href=\"$url\">$nouveauBar</a>";

		$monTableau[$nouveauBar] = 0;
	}

	// 3 : Écriture du fichier
	$pointeurFichier = fopen($monFichier, 'w')
    	or die("Impossible d'ouvrir le fichier $monFichier !");

	foreach ($monTableau as $bar => $nombre) {
		$mesDonnees = "$bar|$nombre\n";
    	fputs($pointeurFichier, $mesDonnees);
	}
	fclose($pointeurFichier)
?>

<!DOCTYPE html>
<html lang="fr" xml:lang="fr">
<head>
  <meta charset="UTF-8" />
  <meta name="author" content="Mathieu MANGEOT" />
  <meta name="keywords" content="STEEP"/>
  <meta name="description"
  content="Classement top dix STEEP" />
  <title>STEEP : top dix des ressources à consulter</title>
  <link href="TP.css" rel="stylesheet" type="text/css" />
</head>

<body>
<header>
<div class="logo-gauche">
<img alt="Logo STEEP" src="Logo-STEEP.png" width="120" /></div>

<h1 class="matiere">Équipe STEEP</h1>

</header>
<section>
<h2>Classement des vidéos à regarder en priorité</h2>

<p>L'objectif est d'établir un classement des vidéos à voir absolument lorsqu'on arrive dans l'équipe STEEP.
	Il est possible de rajouter d'autres vidéos si celles pour lesquelles vous souhaitez voter manquent.</p>
<p></p>
<p>Le formulaire HTML affiche une liste de vidéos. Pour chaque vidéo, une case à cocher est affichée.
Un champ texte est également ajouté pour permettre d'ajouter une nouvelle vidéo qui n'est pas dans la liste.</p>
<p>Consultez la liste des vidéos présentes. Si certaines vidéos que vous pensez indispensables ne sont pas dans la liste, ajoutez-les.
Ensuite, cochez au minimum une vidéo et au maximum 10 vidéos pour faire vore classement. Ne votez qu'une seule fois.</p>
<?php
	if (!empty($nouveauBar)) {
		echo '<p class="nouveau">Voici la nouvelle vidéo ajoutée : ', $nouveauBar,'</p>';
	}
	if (!empty($_REQUEST['NouveauBar']) && preg_match('/</',$_REQUEST['NouveauBar'])) {
		echo '<p class="nouveau">Dis donc, tu te crois malin ?</p>';
	}

/*	if (!empty($_REQUEST['bars'])) {
		echo '<p class="nouveau">';
		echo 'Voici la liste des vidéos sélectionnées : ';
		foreach ($_REQUEST['bars'] as $numero => $bar) {
				echo $bar , ', ';
		}
		echo '</p>';
	}*/
?>
<form action="" method="post">
 <fieldset><legend>Vidéos prioritaires</legend>
 <p>Cochez les vidéos que les nouveaux arrivants à STEEP doivent visionner en priorité :</p>
	<?php
		foreach ($monTableau as $bar => $nombre) {
			$barEscaped = htmlspecialchars($bar);
			echo '<p>[',$nombre,']<input type="checkbox" name="bars[]" value="',$barEscaped,'"/>',$bar,'</p>',"\n";
		}
?>
<p><input type="submit" name="Voter" value="Voter" /></p>

 <p>Ajoutez une nouvelle vidéo que vous avez visionnée si elle n'est pas dans la liste. Ajoutez l'auteur au début, puis ' : ', puis le titre. Ajoutez ensuite son adresse URL dans le deuxième champ :<br/>
	 Titre : <input type="text" name="NouveauBar" required="required" size="100" placeholder="Emmanuel Prados : Comprendre les phénomènes d’effondrement de sociétés. Quel avenir pour la nôtre ?" /><br/>
	 URL : <input type="url" name="NouveauURL" required="required" size="100" placeholder="https://youtu.be/1dgjIeR5DBY" />
 </p>
 <p><input type="submit" name= "Envoyer" value="Envoyer" /></p>
 </fieldset>
</form>
</section>
<footer>
<p>Copyright Mathieu Mangeot 2014-2022, Licence <a href="http://creativecommons.org/licenses/by/3.0/fr/">Creative Commons By 3.0 fr</a></p>
</footer>
</body>
</html>
