<?php
	$monFichier = 'listeTextes.txt';
	$monTableau = array();

	// 1 : Lecture du fichier
	if (file_exists($monFichier)) {
		$pointeurFichier = fopen($monFichier, 'r')
			or die("Impossible d'ouvrir le fichier $monFichier !");
			while (!feof($pointeurFichier)) {
				$ligneFichier=trim(fgets($pointeurFichier));
				if (!empty($ligneFichier)) {
					$ligne = explode('|',$ligneFichier);
					$monTableau[$ligne[0]]=intval($ligne[1]);
			}
		}
	}
	fclose($pointeurFichier);
	arsort($monTableau,SORT_NUMERIC);

	// 2 : Lecture des paramètres
	if (!empty($_REQUEST['Voter']) && !empty($_REQUEST['bars'])) {
		foreach ($_REQUEST['bars'] as $numero => $bar) {
			$bardecode = htmlspecialchars_decode($bar);
			$monTableau[$bar]++;
		}
	}
	$nouveauBar = '';
	if (!empty($_REQUEST['EnvoyerRef']) && !empty($_REQUEST['NouveauTitre']) && !empty($_REQUEST['NouveauAuteur'])) {
		$nouveauBar = '<titre>' . htmlspecialchars($_REQUEST['NouveauTitre']) . '</titre><auteur>' . htmlspecialchars($_REQUEST['NouveauAuteur']) . '</auteur><date>';
		$nouveauBar .= !empty($_REQUEST['NouveauDate'])?htmlspecialchars($_REQUEST['NouveauDate']):'?';
		$nouveauBar .= '</date><ref>';
		$nouveauBar .= !empty($_REQUEST['NouveauRef'])?htmlspecialchars($_REQUEST['NouveauRef']):'?';
		$nouveauBar .= '</ref>';
		$nouveauBar .= !empty($_REQUEST['NouveauURL'])?'<url>'.htmlspecialchars($_REQUEST['NouveauURL']).'</url>':'';
		$monTableau[$nouveauBar] = 0;
	}
	if (!empty($_REQUEST['EnvoyerBib']) && !empty($_REQUEST['NouveauBib'])) {
		$bib = $_REQUEST['NouveauBib'];
		$matches = array();
		$ref = '<ref>';
		if (preg_match('/Booktitle *= *{(.*)}+/i',$bib,$matches)) {
			$ref .= $matches[1] . ', ';
			$bib = preg_replace('/Booktitle *= *{(.*)}+/i','',$bib);
		}
		if (preg_match('/Journal *= *{(.*)}+/i',$bib,$matches)) {
			$ref .= $matches[1] . ', ';
		}
		if (preg_match('/Volume *= *{(.*)}+/i',$bib,$matches)) {
			$ref .= $matches[1] . ', ';
		}
		if (preg_match('/Number *= *{(.*)}+/i',$bib,$matches)) {
			$ref .= '('.$matches[1] . '), ';
		}
		if (preg_match('/School *= *{(.*)}+/i',$bib,$matches)) {
			$ref .= $matches[1] . ', ';
		}
		if (preg_match('/Address *= *{(.*)}+/i',$bib,$matches)) {
			$ref .= $matches[1] . ', ';
		}
		if (preg_match('/Pages *= *{([^}]*)}/i',$bib,$matches)) {
			$ref .= $matches[1] . ' p.';
			var_dump($matches);
		}
		else {
			echo 'nop:',$bib;
		}
		preg_match('/Title *= *{(.*)}+/i',$bib,$matches);
		var_dump($matches);
		$nouveauBar = '<titre>' . $matches[1] . '</titre>';
		preg_match('/Author *= *{(.*)}+/i',$bib,$matches);
		$nouveauBar .= '<auteur>' . $matches[1] . '</auteur>';
		if (preg_match('/Year *= *{([^}]*)}/i',$bib,$matches)) {
			$nouveauBar .= '<date>' . $matches[1] . '</date>';
		}
		else {
			$nouveauBar .= '<date>?</date>';
		}
		$nouveauBar .= $ref.'</ref>';
		if (preg_match('/Url *= *{(.*)}}?/i',$bib,$matches)) {
			$nouveauBar .= '<url>' . $matches[1] . '</url>';
		}
		echo 'nb:<pre>',$nouveauBar,'</pre>';

		$monTableau[$nouveauBar] = 0;
	}

	// 3 : Écriture du fichier
	$pointeurFichier = fopen($monFichier, 'w')
    	or die("Impossible d'ouvrir le fichier $monFichier !");

	foreach ($monTableau as $bar => $nombre) {
		$mesDonnees = "$bar|$nombre\n";
    	fputs($pointeurFichier, $mesDonnees);
	}
	fclose($pointeurFichier)
?>

<!DOCTYPE html>
<html lang="fr" xml:lang="fr">
<head>
  <meta charset="UTF-8" />
  <meta name="author" content="Mathieu MANGEOT" />
  <meta name="keywords" content="STEEP"/>
  <meta name="description"
  content="Classement top dix STEEP" />
  <title>STEEP : top dix des ressources à consulter</title>
  <link href="TP.css" rel="stylesheet" type="text/css" />
</head>

<body>
<header>
<div class="logo-gauche">
<img alt="Logo STEEP" src="Logo-STEEP.png" width="120" /></div>

<h1 class="matiere">Équipe STEEP</h1>

</header>
<section>
<h2>Classement des livres et articles à lire en priorité</h2>

<p>L'objectif est d'établir un classement des livres et articles à lire absolument lorsqu'on arrive dans l'équipe STEEP.
	Il est possible de rajouter d'autres livres et articles si celles pour lesquelles vous souhaitez voter manquent.</p>
<p></p>
<p>Le formulaire HTML affiche une liste de livres et articles. Pour chacun, une case à cocher est affichée.
Un champ texte est également ajouté pour permettre d'ajouter une nouvelle référence qui n'est pas dans la liste.</p>
<p>Consultez la liste des références présentes. Si certaines références que vous pensez indispensables ne sont pas dans la liste, ajoutez-les.
Ensuite, cochez au minimum une référence et au maximum 10 références pour faire vore classement. Ne votez qu'une seule fois.</p>
<?php
	/*if (!empty($nouveauBar) ) {
		echo '<p class="nouveau">Voici la nouvelle référence ajoutée : ', $nouveauBar,'</p>';
	}*/

/*	if (!empty($_REQUEST['bars'])) {
		echo '<p class="nouveau">';
		echo 'Voici la liste des vidéos sélectionnées : ';
		foreach ($_REQUEST['bars'] as $numero => $bar) {
				echo $bar , ', ';
		}
		echo '</p>';
	}*/
?>
<form action="" method="post">
 <fieldset><legend>Références prioritaires</legend>
 <p>Cochez les références que les nouveaux arrivants à STEEP devraient selon vous lire en priorité :</p>
	<?php
		foreach ($monTableau as $bar => $nombre) {
			$barEscaped = htmlspecialchars($bar);
			$bar = preg_replace('/<\/[^>]+>/','</span> ',$bar);
			$bar = preg_replace('/<([^\/][^>]+)>/','<span class="$1">',$bar);
			$bar = preg_replace('/<span class="url">([^<]+)<\/span>/','<a href="$1">$1</a>',$bar);

			echo '<p>[',$nombre,']<input type="checkbox" name="bars[]" value="',$barEscaped,'"/>',$bar,'</p>',"\n";
		}
?>
<p><input type="submit" name="Voter" value="Voter" /></p>
</fieldset>
</form>
<form action="" method="post">
 <fieldset><legend>Nouvelle référence</legend>
<p>Ajoutez une nouvelle référence que vous avez lue si elle n'est pas dans la liste. L'adresse URL est optionnelle.<br/>
			Titre : <input type="text" name="NouveauTitre" required="required" size="80" placeholder="The Limits to Growth" /><br/>
			Auteurs : <input type="text" name="NouveauAuteur"  required="required" size="80" placeholder="Donella H. Meadows et al." /><br/>
			Date : <input type="text" name="NouveauDate"  required="required" size="5" placeholder="1972" /><br/>
			Références : <input type="text" name="NouveauRef"  required="required" size="80" placeholder="Universe Books, 205 p." /><br/>
			URL : <input type="url" name="NouveauURL" size="80" placeholder="https://donellameadows.org/wp-content/userfiles/Limits-to-Growth-digital-scan-version.pdf" />
		</p>
		<p><input type="submit" name= "EnvoyerRef" value="Envoyer" /></p>
		</fieldset>
	</form>
		<form action="" method="post">
		 <fieldset><legend>Nouveau .bib</legend>
		<p>Ou sinon ajoutez un .bib :<br/>
			<textarea name="NouveauBib" required="required" rows="15" cols="100"></textarea>
			<p>
 <p><input type="submit" name= "EnvoyerBib" value="Envoyer" /></p>
 </fieldset>
</form>
</section>
<footer>
<p>Copyright Mathieu Mangeot 2014-2022, Licence <a href="http://creativecommons.org/licenses/by/3.0/fr/">Creative Commons By 3.0 fr</a></p>
</footer>
</body>
</html>
